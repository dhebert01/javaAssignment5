/**
 * This is a skeleton class for cylinders
 * @author David Hebert
 * @version 10/11/2023
 */

package shapes;

public class Cylinder implements Shape3d {
    private double radius;
    private double height;

    public Cylinder(double radius, double height){
    
        if(height < 0 || radius < 0){
            throw new IllegalArgumentException("cant be negative number");
        }
        
        this.radius = radius;
        this.height = height;
        
    }

    public double getVolume(){
        return Math.PI * Math.pow(this.radius, 2) * this.height;
    }

    public double getSurfaceArea(){
        return (2 * Math.PI * Math.pow(this.radius, 2)) + (2 * Math.PI * this.radius + this.height);
    }

    public double getRadius(){
        return this.radius;
    }
    
    public double getHeight(){
        return this.height;
    }
}
