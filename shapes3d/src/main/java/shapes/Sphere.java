/**
 * This is a skeleton class for the sphere class
 * @author Ivana Zhekova
 * @version 10/11/2023
 */


package shapes;

public class Sphere implements Shape3d {
    private double radius;
    
    public Sphere (double radius){
        if (radius <= 0) {
            throw new IllegalArgumentException("Radius must be positive");
        }
        this.radius = radius;

    }

    public double getVolume(){
        return (4.0 / 3.0) * Math.PI * Math.pow(radius, 3);


    }
    public double getSurfaceArea(){
        return 4 * Math.PI * Math.pow(radius, 2); 
    }

    public double getRadius(){
           return radius;
        
    }
    
}
