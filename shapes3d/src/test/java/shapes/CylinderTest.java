/**
 * Unit tests for the Cylinder class.
 * @author Ivana Zhekova
 * @version 10/11/2023
 */



package shapes;

import org.junit.Test;
import static org.junit.Assert.*;

public class CylinderTest {
    @Test
    public void testGetters() {
        Cylinder cylinder = new Cylinder(2.0, 3.0);
        assertEquals(2.0, cylinder.getRadius(), 0.001);
        assertEquals(3.0, cylinder.getHeight(), 0.001);
    }

    @Test
    public void testConstructorWithNegativeValues() {
        try {
            Cylinder negativeCylinder = new Cylinder(-2.0, -3.0);
            fail("Expected an exception for negative values");
        } catch (IllegalArgumentException e) {
            
        }
    }
    

    @Test
    public void testGetVolume() {
        Cylinder cylinder = new Cylinder(2.0, 3.0);
        double expectedVolume = Math.PI * Math.pow(2.0, 2) * 3.0;
        assertEquals(expectedVolume, cylinder.getVolume(), 0.001);
    }

    @Test
    public void testGetVolumeWithNegativeValues() {
        try {
            Cylinder negativeCylinder = new Cylinder(-2.0, -3.0);
            fail("Expected an exception for negative values");
        } catch (IllegalArgumentException e) {
        
        }
    }

    @Test
    public void testGetSurfaceArea() {
        Cylinder cylinder = new Cylinder(2.0, 3.0);
        double expectedSurfaceArea = (2 * Math.PI * Math.pow(2.0, 2)) + (2 * Math.PI * 2.0 + 3.0);
        assertEquals(expectedSurfaceArea, cylinder.getSurfaceArea(), 0.001);
    }

    @Test
    public void testGetSurfaceAreaWithNegativeValues() {
        try {
            Cylinder negativeCylinder = new Cylinder(-2.0, -3.0);
            fail("Expected an exception for negative values");
        } catch (IllegalArgumentException e) {
          
        }
    }


 
}
