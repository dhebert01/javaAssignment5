
/**
 * This is for testing Sphere class
 * @author David
 * @version 10/11/2023
 * */
package shapes;


import org.junit.Test;
import static org.junit.Assert.*;

public class SphereTest{
    @Test
    public void testConstructorNegativeNums(){
        try{
            Sphere a = new Sphere(-5.0);
            fail("Expected negative values to fail");
        }
        catch(IllegalArgumentException e){

        }
    }
    @Test
    public void testGetSurfaceArea() {
        Sphere sphere = new Sphere(5);
        double expectedSurfaceArea = 4 * Math.PI * Math.pow(5, 2);
        assertEquals(expectedSurfaceArea, sphere.getSurfaceArea(), 0.001);
    }
    @Test
    public void testGetVolume() {
        Sphere sphere = new Sphere(5);
        double expectedVolume = (4.0 / 3.0) * Math.PI * Math.pow(5, 3);
        assertEquals(expectedVolume, sphere.getVolume(), 0.001);
    }

   @Test
    public void testGetMethod(){
        Sphere a = new Sphere(5);
        assertEquals(5.0, 5.0, 0.001);
    }

  
}